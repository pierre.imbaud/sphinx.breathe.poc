==================================================
 Poc: Proof of concept for doxygen sphinx breathe
==================================================

This documentation package documents nothing. Nothing, but the ability
of the quoted tools to provide a quality documentation system.

What will, then, be the content? Well, it will consist of:

- Plain nonsense talk; as is, after all, the famous "hello world";
  and, by the way, the even more famous *allo* !
- Illustration of tool gimmicks. And, when it is the case, text will
  comment the illustrated gimmicks.
- Questions asked while developping this POC.

Authors abilities
=================

If I knew well the techniques used here, I would have written a
tutorial, not a poc. I feel pretty fluent with restructuredText, the
base langage, but for the rest:

- sphinx: read the doc, not practiced.
- epidoc: good level, but not master.
- doxygen: very shallow knowledge.
- breathe: beginner.

I hope I shall master the whole chain when done with this poc.

RST
===

The main base used here is rst, or restructuredText.
It comes from the python galaxy, has a rich history, a lot of
possibilities. I consider it the best "light markup langage" ever, and
a very good tradeoff.

Basic tools are provided with python package docutils. 

Most sphinx docs, and eventually this one, hopefully, have a "see
source" button, that can help You see how a particular html rendering
has been produced in rst.

Expected covering
=================

This poc is expected to comprise:

- handwritten sphinx pages.
- python module documentation, extracted from docstrings
- C code documentation, extracted by doxygen, and translated to rst

The seamless merge of these is one of the objectives of the
demonstration. We should be able to have links from any of these
realms to any other.

Let us illustrate here the needs for these links:

C or python functions implement concepts. These concepts are not
(not always (:-) ) directly mapped on functions or classes: concepts
will be discribed in sphinx pages, functions and classes that
implement them need to refer to entries in general documentation.

On the other side, general documentation needs to refer to functions
and classes.


