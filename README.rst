====================
 sphinx breathe poc
====================

This package has a very limited scope: be a demonstrator for the
combination of sphinx and breathe. 

The documentation merged by sphinx will come from 3 sources:

- native .rst files, hand written, hopefully in compliance with rst
  and sphinx rules
- python docstring, extracted from the fake python package.
- c docstring, processed by doxygen, then translated by breathe.

The initial author, Pierre Imbaud, has limited experience with doxygen
and sphinx, and no experience at all with breathe. Hence if You find
inadequate or clumsy constructs here, do not hesitate to fix them.

The python and c code developped here have no purpose, as of this
README writing, but to have docstrings intended for processing by
sphinx or doxygen.

Directory setup
===============

We have 3 main directories:

:sphinx: sphinx files.
:py: python files.
:c: C files



